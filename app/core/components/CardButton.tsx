import { noop } from "lodash"
import React, { ButtonHTMLAttributes, DetailedHTMLProps } from "react"

interface ICardButtonProps
  extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  leftSlot?: React.ReactNode
  middleSlot?: React.ReactNode
  rightSlot?: React.ReactNode
  width?: string
  theme: "primary" | "outline"
  onClick: () => void
  className: string
}

const CardButton: React.FunctionComponent<ICardButtonProps> = ({
  leftSlot,
  middleSlot,
  rightSlot,
  theme,
  className,
  width,
  onClick,
}) => {
  return (
    <div
      onClick={() => onClick()}
      onKeyDown={noop}
      role="button"
      tabIndex={0}
      className={`rounded shadow-md tracking-widest border-black border w-${width} h-28 ${className}`}
    >
      <div className="flex items-center justify-between">
        <div className="ml-5">{leftSlot}</div>
        {middleSlot}
        <div className="mr-5">{rightSlot}</div>
      </div>
    </div>
  )
}

export default CardButton
