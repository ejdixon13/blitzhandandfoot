import * as React from "react"

interface IPageHeaderProps {
  title?: string | React.ReactNode
  button?: React.ReactNode
  centerTitle?: boolean
}

const PageHeader: React.FunctionComponent<IPageHeaderProps> = ({ title, button, centerTitle }) => {
  return (
    <>
      {centerTitle ? (
        <div className="flex items-center h-16 mt-2">
          <div className="absolute left-2">{button}</div>
          <div className="w-full text-center text-xl">{title}</div>
        </div>
      ) : (
        <div className="flex items-center">
          <div>{button}</div>
          <div className="ml-10">{title}</div>
        </div>
      )}
    </>
  )
}

export default PageHeader
