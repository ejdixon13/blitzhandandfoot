import React, { ButtonHTMLAttributes, DetailedHTMLProps } from "react"
import styles from "./Button.module.scss"

interface IButtonProps
  extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  theme: "primary" | "outline" | "circle"
  width?: string
  height?: string
}

const Button: React.FunctionComponent<IButtonProps> = ({
  theme,
  width,
  height,
  className,
  children,
  ...rest
}) => {
  return (
    <button
      {...rest}
      className={`rounded shadow-md tracking-widest ${
        styles["button"]
      } ${className} w-${width} h-${height} ${
        theme === "primary" ? "bg-blue-500 text-white" : ""
      } ${theme === "outline" ? "border-black border" : ""}
      ${theme === "circle" ? "rounded-full" : ""}`}
    >
      {children}
    </button>
  )
}

export default Button
