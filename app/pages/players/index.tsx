import { faChevronLeft, faPlusCircle, faUser } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Button from "app/core/components/Button"
import CardButton from "app/core/components/CardButton"
import PageHeader from "app/core/components/PageHeader"
import Layout from "app/core/layouts/Layout"
import getPlayers from "app/players/queries/getPlayers"
import { BlitzPage, Head, Link, usePaginatedQuery, useRouter } from "blitz"
import { Suspense, useState } from "react"

const ITEMS_PER_PAGE = 100
interface Team {
  player1: number | null
  player2: number | null
}

export const PlayersList = () => {
  const [{ players, hasMore }] = usePaginatedQuery(getPlayers, {
    orderBy: { id: "asc" },
    take: ITEMS_PER_PAGE,
  })

  const [team1, setTeam1] = useState<Team>({ player1: null, player2: null })
  const [team2, setTeam2] = useState<Team>({ player1: null, player2: null })

  const addToTeam = (id: number) => {
    console.log("Add to Team")
    if (!team1.player1) {
      setTeam1((_team1) => ({
        player1: id,
        player2: _team1?.player2 || null,
      }))
    } else if (!team1.player2) {
      setTeam1((_team1) => ({
        player1: _team1?.player1 || null,
        player2: id,
      }))
    } else if (!team2.player1) {
      setTeam2((_team2) => ({
        player1: id,
        player2: _team2?.player2 || null,
      }))
    } else {
      setTeam2((_team2) => ({
        player1: _team2?.player1 || null,
        player2: id,
      }))
    }
  }

  const switchTeam = (id: number) => {
    if (team1.player1 === id || team1.player2 === id) {
      if (!team2.player1) {
        setTeam2((_team2) => ({
          ..._team2,
          player1: id,
        }))
      } else {
        setTeam2((_team2) => ({
          ..._team2,
          player2: id,
        }))
      }
    } else {
      if (!team1.player1) {
        setTeam1((_team1) => ({
          ..._team1,
          player1: id,
        }))
      } else {
        setTeam1((_team1) => ({
          ..._team1,
          player2: id,
        }))
      }
    }
  }

  return (
    <div className="text-center mt-8">
      <ul>
        <li className="mb-6">
          <CardButton
            theme="outline"
            width="96"
            leftSlot={<FontAwesomeIcon className="w-8" icon={faPlusCircle} />}
            middleSlot={
              <div>
                <Link href="/players/new">
                  <strong>Add a Player</strong>
                </Link>
              </div>
            }
          />
        </li>
        {players.map((player) => (
          <li className="mb-2" key={player.id}>
            <CardButton
              onClick={() => addToTeam(player.id)}
              className={`${
                team1.player1 === player.id || team1.player2 === player.id ? "bg-blue-200" : ""
              }`}
              theme="outline"
              width="96"
              leftSlot={<FontAwesomeIcon className="w-8" icon={faUser} />}
              middleSlot={<div>{player.name}</div>}
            />
          </li>
        ))}
      </ul>
      <div className="w-full text-center mt-5">
        <Button theme="primary" width="56" height="14">
          Start Game
        </Button>
      </div>
    </div>
  )
}

const PlayersPage: BlitzPage = () => {
  const router = useRouter()
  const goHome = () => router.push("/")
  return (
    <>
      <Head>
        <title>Players</title>
      </Head>

      <div>
        <PageHeader
          title="Choose Players"
          centerTitle
          button={
            <Button
              width="28"
              height="10"
              onClick={goHome}
              theme="outline"
              className="flex items-center justify-evenly"
            >
              <FontAwesomeIcon className="w-3 mr-2" icon={faChevronLeft} />
              Back
            </Button>
          }
        />

        <Suspense fallback={<div>Loading...</div>}>
          <PlayersList />
        </Suspense>
      </div>
    </>
  )
}

PlayersPage.authenticate = true
PlayersPage.getLayout = (page) => <Layout>{page}</Layout>

export default PlayersPage
