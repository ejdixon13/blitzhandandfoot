import Layout from "app/core/layouts/Layout"
import { FORM_ERROR, PlayerForm } from "app/players/components/PlayerForm"
import createPlayer from "app/players/mutations/createPlayer"
import { BlitzPage, Link, useMutation, useRouter } from "blitz"
import styles from "./new.module.scss"

const NewPlayerPage: BlitzPage = () => {
  const router = useRouter()
  const [createPlayerMutation] = useMutation(createPlayer)

  return (
    <div>
      <h1 className={styles["turnBlue"]}>Create New Player</h1>
      <PlayerForm
        submitText="Create Player"
        // TODO use a zod schema for form validation
        //  - Tip: extract mutation's schema into a shared `validations.ts` file and
        //         then import and use it here
        // schema={CreatePlayer}
        // initialValues={{}}
        onSubmit={async (values) => {
          try {
            const player = await createPlayerMutation(values)
            router.push(`/players/${player.id}`)
          } catch (error) {
            console.error(error)
            return {
              [FORM_ERROR]: error.toString(),
            }
          }
        }}
      />

      <p>
        <Link href="/players">
          <a>Players</a>
        </Link>
      </p>
    </div>
  )
}

NewPlayerPage.authenticate = true
NewPlayerPage.getLayout = (page) => <Layout title={"Create New Player"}>{page}</Layout>

export default NewPlayerPage
