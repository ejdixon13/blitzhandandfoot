import { faHandSpock, faShoePrints, faUser } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import logout from "app/auth/mutations/logout"
import Button from "app/core/components/Button"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import Layout from "app/core/layouts/Layout"
import { BlitzPage, Link, useMutation, useRouter } from "blitz"
import { Suspense } from "react"

/*
 * This file is just for a pleasant getting started page for your new app.
 * You can delete everything in here and start from scratch if you like.
 */

const UserInfoAndButtons = () => {
  const currentUser = useCurrentUser()
  const [logoutMutation] = useMutation(logout)
  const router = useRouter()

  const Buttons = () => (
    <>
      <Button
        theme="primary"
        width="56"
        height="12"
        className="my-4"
        disabled={!currentUser}
        onClick={() => router.push("/players")}
      >
        New Game
      </Button>
      <Button theme="outline" width="56" height="12" disabled={!currentUser}>
        View Past Games
      </Button>
      {/* <Dialog onClose={() => setOpen(false)} aria-labelledby="simple-dialog-title" open={open}>
        <DialogTitle id="simple-dialog-title">Set backup account</DialogTitle>
      </Dialog> */}
    </>
  )

  if (currentUser) {
    return (
      <>
        <Buttons />
        <div className="absolute bottom-5">
          <div className="flex">
            <FontAwesomeIcon className="w-4 mr-2 text-blue-800" icon={faUser} />
            {currentUser.email}
          </div>
          <button
            className="underline text-blue-400"
            onClick={async () => {
              await logoutMutation()
            }}
          >
            Logout
          </button>
        </div>
      </>
    )
  } else {
    return (
      <>
        <Buttons />
        <div className="mt-5">
          <Link href="/signup">
            <a className="underline text-blue-400">
              <strong>Sign Up</strong>
            </a>
          </Link>
          <br />
          <Link href="/login">
            <a className="underline text-blue-400">
              <strong>Login</strong>
            </a>
          </Link>
        </div>
      </>
    )
  }
}

const Home: BlitzPage = () => {
  return (
    <div className="container">
      <main className="text-center flex flex-col justify-center items-center h-screen">
        <div className="w-28 flex items-center mb-20">
          <FontAwesomeIcon className="text-red-600 relative -top-5 -left-4" icon={faHandSpock} />
          <span className="text-5xl text-gray-300">&</span>
          <FontAwesomeIcon
            className="text-red-300 relative -bottom-5 -right-4"
            icon={faShoePrints}
          />
        </div>
        <Suspense fallback="Loading...">
          <UserInfoAndButtons />
        </Suspense>
      </main>

      <style jsx global>{`
        @import url("https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;700&display=swap");
      `}</style>
    </div>
  )
}

Home.suppressFirstRenderFlicker = true
Home.getLayout = (page) => <Layout title="Home">{page}</Layout>

export default Home
